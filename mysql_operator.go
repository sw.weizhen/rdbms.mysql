package swmysql

import (
	"errors"
	"strings"
)

func (ref *DBOperator) IsConn() bool {
	return ref.isConn
}

func (ref *DBOperator) SetMaxIdleConns(conns int) {
	ref.conn.SetMaxIdleConns(conns)
}

func (ref *DBOperator) SetMaxOpenConns(conns int) {
	ref.conn.SetMaxOpenConns(conns)
}

func (ref *DBOperator) CheckDBStatus() bool {
	if ref.conn == nil {
		return false
	}

	return ref.conn.Ping() == nil
}

func (ref *DBOperator) Close() {
	if ref.conn != nil {
		ref.conn.Close()
	}
}

func (ref *DBOperator) Exec(sqlCmd string, args ...interface{}) (int64, error) {
	if ref.conn == nil {
		ref.errMsg = "nil db connection"
		return 0, errors.New(ref.errMsg)
	}

	if len(sqlCmd) == 0 {
		ref.errMsg = "empty sql cmd"
		return 0, errors.New(ref.errMsg)
	}

	resp, err := processSQLExec(sqlCmd, ref.conn, args...)

	if err != nil {
		return 0, err
	}

	if strings.HasPrefix(strings.Trim(strings.ToLower(sqlCmd), " "), "insert") {
		return resp.LastInsertId()
	}

	return resp.RowsAffected()
}

func (ref *DBOperator) Query(sqlCmd string, args ...interface{}) (*DBResponse, error) {
	if len(sqlCmd) == 0 {
		ref.errMsg = "empty sql cmd"
		return nil, errors.New(ref.errMsg)
	}

	if ref.conn == nil {
		ref.errMsg = "nil db connection"
		return nil, errors.New(ref.errMsg)
	}

	rtnRowsMap, err := innerQueryParsing(sqlCmd, ref.conn, args...)
	if err != nil {
		return nil, err
	}

	var response DBResponse
	response.RowsResponse = make([]map[string]string, 0)
	response.RowsResponse = append(response.RowsResponse, rtnRowsMap...)

	response.Length = uint32(len(response.RowsResponse))
	return &response, nil
}

func (ref *DBOperator) ResultSet(sqlCmd string, args ...interface{}) ([]DBResponse, error) {
	if len(sqlCmd) == 0 {
		ref.errMsg = "empty sql cmd"
		return nil, errors.New(ref.errMsg)
	}

	if ref.conn == nil {
		ref.errMsg = "nil db connection"
		return nil, errors.New(ref.errMsg)
	}

	rtnRowsMap, err := procResultSetParsing(sqlCmd, ref.conn, args...)
	if err != nil {
		return nil, err
	}

	var responseSlice []DBResponse = make([]DBResponse, len(rtnRowsMap))
	for idx, val := range rtnRowsMap {
		var response DBResponse
		response.RowsResponse = make([]map[string]string, 0)
		response.RowsResponse = append(response.RowsResponse, val...)

		response.Length = uint32(len(response.RowsResponse))

		responseSlice[idx] = response
	}

	return responseSlice, nil
}

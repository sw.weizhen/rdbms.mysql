package swmysql

import (
	"database/sql"
	"errors"
	"fmt"
)

func innerQueryParsing(sqlCmd string, db *sql.DB, args ...interface{}) ([]map[string]string, error) {
	rows, err := processSQLQuery(sqlCmd, db, args...)
	if err != nil {
		return nil, err
	}

	return queryRtnParsing(rows), nil
}

func processSQLQuery(sqlCmd string, db *sql.DB, args ...interface{}) (*sql.Rows, error) {
	sqlCmdType, err := sqlcmdType(sqlCmd, args...)
	if err != nil {
		return nil, err
	}

	var rows *sql.Rows
	switch sqlCmdType {
	case PLAINTEXT:
		rows, err = db.Query(sqlCmd)

	case PLAINTEXTWITHARGS:
		rows, err = db.Query(fmt.Sprintf(sqlCmd, args...))

	case PREPARED:
		rows, err = db.Query(sqlCmd, args...)

	default:
		return nil, errors.New("undefined sql type")
	}

	return rows, err
}

func processSQLExec(sqlCmd string, db *sql.DB, args ...interface{}) (sql.Result, error) {
	sqlCmdType, err := sqlcmdType(sqlCmd, args...)
	if err != nil {
		return nil, err
	}

	var resp sql.Result
	switch sqlCmdType {
	case PLAINTEXT:
		resp, err = db.Exec(sqlCmd)

	case PLAINTEXTWITHARGS:
		resp, err = db.Exec(fmt.Sprintf(sqlCmd, args...))

	case PREPARED:
		resp, err = db.Exec(sqlCmd, args...)

	default:
		return nil, errors.New("undefined sql type")
	}

	return resp, err
}

func procResultSetParsing(sqlCmd string, db *sql.DB, args ...interface{}) ([][]map[string]string, error) {
	rows, err := processSQLQuery(sqlCmd, db, args...)
	if err != nil {
		return nil, err
	}

	return resultSetParsing(rows), nil
}

func resultSetParsing(rows *sql.Rows) [][]map[string]string {
	defer rows.Close()

	var mpResultSet [][]map[string]string = make([][]map[string]string, 0)

	for {
		columns, _ := rows.Columns()
		columnsCnt := len(columns)
		columnsPtr := make([]interface{}, columnsCnt)
		columnsVal := make([]interface{}, columnsCnt)

		for index := range columnsVal {
			columnsPtr[index] = &columnsVal[index]
		}

		rtnRowsMap := make([]map[string]string, 0)
		for rows.Next() {
			oneRowMap := make(map[string]string)
			rows.Scan(columnsPtr...)
			for index, column := range columnsVal {
				if column == nil {
					continue
				}
				oneRowMap[columns[index]] = string(column.([]byte))
			}

			rtnRowsMap = append(rtnRowsMap, oneRowMap)
		}

		mpResultSet = append(mpResultSet, rtnRowsMap)

		if !rows.NextResultSet() {
			break
		}
	}

	return mpResultSet
}

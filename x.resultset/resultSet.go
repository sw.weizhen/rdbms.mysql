package resultset

import (
	"database/sql/driver"
	"sync"
	"time"
)

type driverStmt struct {
	sync.Locker
	si       driver.Stmt
	closed   bool
	closeErr error
}

type DepSet map[interface{}]bool

type finalCloser interface {
	finalClose() error
}

type connRequest struct {
	conn *driverConn
	err  error
}

type DB struct {
	waitDuration int64

	connector driver.Connector
	numClosed uint64

	mu           sync.Mutex
	freeConn     []*driverConn
	connRequests map[uint64]chan connRequest
	nextRequest  uint64
	numOpen      int

	openerCh          chan struct{}
	closed            bool
	dep               map[finalCloser]DepSet
	lastPut           map[*driverConn]string
	maxIdleCount      int
	maxOpen           int
	maxLifetime       time.Duration
	maxIdleTime       time.Duration
	cleanerCh         chan struct{}
	waitCount         int64
	maxIdleClosed     int64
	maxIdleTimeClosed int64
	maxLifetimeClosed int64

	stop func()
}

type driverConn struct {
	db        *DB
	createdAt time.Time

	sync.Mutex
	ci          driver.Conn
	needReset   bool
	closed      bool
	finalClosed bool
	openStmt    map[*driverStmt]bool

	inUse      bool
	returnedAt time.Time
	onPut      []func()
	dbmuClosed bool
}

type Rows struct {
	dc          *driverConn
	releaseConn func(error)
	rowsi       driver.Rows
	cancel      func()
	closeStmt   *driverStmt

	closemu  sync.RWMutex
	closed   bool
	lasterr  error
	lastcols []driver.Value
}

func (rs *Rows) NextResultSet() bool {
	var doClose bool
	defer func() {
		if doClose {
			rs.Close()
		}
	}()
	rs.closemu.RLock()
	defer rs.closemu.RUnlock()

	if rs.closed {
		return false
	}

	rs.lastcols = nil
	nextResultSet, ok := rs.rowsi.(driver.RowsNextResultSet)
	if !ok {
		doClose = true
		return false
	}

	rs.dc.Lock()
	defer rs.dc.Unlock()

	rs.lasterr = nextResultSet.NextResultSet()
	if rs.lasterr != nil {
		doClose = true
		return false
	}
	return true
}

func (rs *Rows) Close() error {
	return rs.close(nil)
}

func (rs *Rows) close(err error) error {
	rs.closemu.Lock()
	defer rs.closemu.Unlock()

	if rs.closed {
		return nil
	}
	rs.closed = true

	if rs.lasterr == nil {
		rs.lasterr = err
	}

	withLock(rs.dc, func() {
		err = rs.rowsi.Close()
	})
	if fn := rowsCloseHook(); fn != nil {
		fn(rs, &err)
	}
	if rs.cancel != nil {
		rs.cancel()
	}

	if rs.closeStmt != nil {
		rs.closeStmt.Close()
	}
	rs.releaseConn(err)
	return err
}

func withLock(lk sync.Locker, fn func()) {
	lk.Lock()
	defer lk.Unlock() // in case fn panics
	fn()
}

var rowsCloseHook = func() func(*Rows, *error) { return nil }

func (ds *driverStmt) Close() error {
	ds.Lock()
	defer ds.Unlock()
	if ds.closed {
		return ds.closeErr
	}
	ds.closed = true
	ds.closeErr = ds.si.Close()
	return ds.closeErr
}

package swmysql

import (
	"database/sql"
	"errors"
	"fmt"
)

func innerTxQueryParsing(sqlCmd string, tx *sql.Tx, args ...interface{}) ([]map[string]string, error) {

	rows, err := processSQLTXQuery(sqlCmd, tx, args...)

	if err != nil {
		return nil, err
	}

	return queryRtnParsing(rows), nil
}

func processSQLTXQuery(sqlCmd string, tx *sql.Tx, args ...interface{}) (*sql.Rows, error) {
	sqlCmdType, err := sqlcmdType(sqlCmd, args...)
	if err != nil {
		return nil, err
	}

	var rows *sql.Rows
	switch sqlCmdType {
	case PLAINTEXT:
		rows, err = tx.Query(sqlCmd)

	case PLAINTEXTWITHARGS:
		rows, err = tx.Query(fmt.Sprintf(sqlCmd, args...))

	case PREPARED:
		rows, err = tx.Query(sqlCmd, args...)

	default:
		return nil, errors.New("undefined sql type")
	}

	return rows, err
}

func processSQLTXExec(sqlCmd string, tx *sql.Tx, args ...interface{}) (sql.Result, error) {
	sqlCmdType, err := sqlcmdType(sqlCmd, args...)
	if err != nil {
		return nil, err
	}

	var resp sql.Result
	switch sqlCmdType {
	case PLAINTEXT:
		resp, err = tx.Exec(sqlCmd)

	case PLAINTEXTWITHARGS:
		resp, err = tx.Exec(fmt.Sprintf(sqlCmd, args...))

	case PREPARED:
		resp, err = tx.Exec(sqlCmd, args...)

	default:
		return nil, errors.New("undefined sql type")
	}

	return resp, err
}

func innerTxResultSetParsing(sqlCmd string, tx *sql.Tx, args ...interface{}) ([][]map[string]string, error) {
	rows, err := processSQLTXQuery(sqlCmd, tx, args...)

	if err != nil {
		return nil, err
	}

	return resultSetParsing(rows), nil
}

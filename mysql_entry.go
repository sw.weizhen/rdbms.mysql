package swmysql

import (
	"database/sql"
	"fmt"
)

func New(user, pass, host string, port int, database, charset string, maxIdleConns, maxOpenConns int) (*DBOperator, error) {

	/*
		 ****** interpolateParams ******
		If interpolateParams is true, placeholders (?) in calls to db.Query() and db.Exec() are interpolated into a single query string with given parameters.
		This reduces the number of roundtrips, since the driver has to prepare a statement, execute it with given parameters and close the statement again
		with interpolateParams=false.

		****** multiStatements ******
		Allow multiple statements in one query. While this allows batch queries, it also greatly increases the risk of SQL injections.
		Only the result of the first query is returned, all other results are silently discarded.
		When multiStatements is used, ? parameters must only be used in the first statement.
	*/

	// if multiStatements {
	// 	interpolateParams = true
	// }

	openArgs := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&interpolateParams=true&multiStatements=true", user, pass, host, port, database, charset)

	sqlDB, err := sql.Open("mysql", openArgs)
	if err != nil {
		return nil, err
	}

	sqlDB.SetMaxIdleConns(maxIdleConns)
	sqlDB.SetMaxOpenConns(maxOpenConns)

	err = sqlDB.Ping()
	if err != nil {
		return nil, err
	}

	return &DBOperator{
		conn:     sqlDB,
		database: database,
		isConn:   true,
	}, nil
}

package swmysql

import (
	"errors"
	"strings"
)

func (ref *DBOperator) CommenceTransaction() (*DBTransaction, error) {
	ftTx, err := ref.conn.Begin()
	if err != nil {
		return nil, err
	}

	return &DBTransaction{
		tx:     ftTx,
		DBName: ref.database,
	}, nil
}

func (ref *DBTransaction) Rollback() error {
	return ref.tx.Rollback()
}

func (ref *DBTransaction) Commit() error {
	return ref.tx.Commit()
}

func (ref *DBTransaction) Query(sqlCmd string, args ...interface{}) (*DBResponse, error) {
	if sqlCmd == "" {
		return nil, errors.New("empty sql cmd")
	}

	if ref.tx == nil {
		return nil, errors.New("nil tx point")
	}

	rtnRowsMap, err := innerTxQueryParsing(sqlCmd, ref.tx, args...)
	if err != nil {
		return nil, err
	}

	var response DBResponse
	response.RowsResponse = make([]map[string]string, 0)

	response.RowsResponse = append(response.RowsResponse, rtnRowsMap...)
	response.Length = uint32(len(response.RowsResponse))
	return &response, nil
}

func (ref *DBTransaction) Exec(sqlCmd string, args ...interface{}) (int64, error) {
	if sqlCmd == "" {
		return 0, errors.New("empty sql cmd")
	}

	if ref.tx == nil {
		return 0, errors.New("nil tx point")
	}

	resp, err := processSQLTXExec(sqlCmd, ref.tx, args...)

	if err != nil {
		return 0, err
	}

	if strings.HasPrefix(strings.ToLower(sqlCmd), "insert") {
		return resp.LastInsertId()
	}

	return resp.RowsAffected()
}

func (ref *DBTransaction) ResultSet(sqlCmd string, args ...interface{}) ([]DBResponse, error) {
	if sqlCmd == "" {
		return nil, errors.New("empty sql cmd")
	}

	if ref.tx == nil {
		return nil, errors.New("nil tx point")
	}

	rtnRowsMap, err := innerTxResultSetParsing(sqlCmd, ref.tx, args...)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	var responseSlice []DBResponse = make([]DBResponse, len(rtnRowsMap))
	for idx, val := range rtnRowsMap {
		var response DBResponse
		response.RowsResponse = make([]map[string]string, 0)
		response.RowsResponse = append(response.RowsResponse, val...)

		response.Length = uint32(len(response.RowsResponse))

		responseSlice[idx] = response
	}

	return responseSlice, nil
}

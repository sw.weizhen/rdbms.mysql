package main

import (
	"fmt"
	"runtime"
	"strings"
	"sync"
	"time"

	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"
)

func init() {
	dbLocal, err = swmysql.New("root", "123456", "127.0.0.1", 3306, "sw", "utf8mb4", 2, 20)
	dbRemote, err = swmysql.New("root", "123456", "192.168.33.79", 3306, "KYDB_NEW", "utf8mb4", 2, 20)

}

const (
	loop       = 500000
	loopRemote = 80000
)

var dbLocal *swmysql.DBOperator
var dbRemote *swmysql.DBOperator

var err error

var wg sync.WaitGroup

func main() {

	if err != nil {
		fmt.Printf("fatal error: %v\n", err)
		return
	}

	defer func() {
		fmt.Println("mysql close")

		// dbLocal.Close()
		dbRemote.Close()
	}()

	// SelCost()

	resp, err := dbRemote.ResultSet("call KYDB_NEW.sp_gameRecordSort(0, '', '2021-10-21', '2021-10-22', '0', '0', '0', 0, false, '0');")
	if err != nil {
		fmt.Printf("query err: %s\n", err)
		return
	}

	fmt.Println(resp[0])
	fmt.Println(len(resp))
	fmt.Println(resp[1])

	tx, err := dbRemote.CommenceTransaction()
	if err != nil {
		fmt.Printf("tx error: %s\n", err)
		return
	}

	res, err := tx.ResultSet("call KYDB_NEW.sp_gameRecordSort(0, '', '2021-10-21', '2021-10-22', '0', '0', '0', 0, false, '0');")
	if err != nil {
		fmt.Printf("result set failed: %s\n", err.Error())
		return
	}

	for i, v := range res {
		fmt.Printf("no. %d : %v\n", i, v)

	}

	tx.Commit()
	fmt.Println("done")

}

func Sel() {
	var sbSQL strings.Builder
	sbSQL.WriteString("SELECT title, author FROM books b where b.id=? or b.id=? ;")
	// sbSQL.WriteString("SELECT title, author FROM books b where b.id=? or b.id=? ;")

	resp, err := dbLocal.Query(sbSQL.String(), 1, 2)
	if err != nil {
		fmt.Printf("db query error: %v\n", err)
		return
	}

	fmt.Printf("response count: %v\n, data: %v\n", resp.Length, resp.RowsResponse)
}

func SelCost() {
	t := time.Now().Unix()
	fmt.Printf("SelCost::num of goroutine: %v\n", runtime.NumGoroutine())

	wg.Add(loop)
	for i := 0; i < loop; i++ {
		go func() {
			queryLocal()
			wg.Done()
		}()
	}
	fmt.Printf("SelCost::num of goroutine: %v\n", runtime.NumGoroutine())
	wg.Wait()

	fmt.Printf("SelCost: %vs\n", time.Now().Unix()-t)
}

func Ins() {
	var sbSQL strings.Builder
	sbSQL.WriteString("INSERT INTO books (title, author) VALUES('Mathematics for Machine Learning', 'S.W.');")
	sbSQL.WriteString("INSERT INTO books (title, author) VALUES('Golang', 'S.W.');")
	sbSQL.WriteString("INSERT INTO books (title, author) VALUES('C#', 'S.W.');")
	sbSQL.WriteString("INSERT INTO books (title, author) VALUES('Java', 'S.W.');")
	sbSQL.WriteString("INSERT INTO books (title, author) VALUES('Ruby', 'S.W.');")
	sbSQL.WriteString("INSERT INTO books (title, author) VALUES('C/C++', 'S.W.');")
	sbSQL.WriteString("INSERT INTO books (title, author) VALUES('SEGA Mega Drive', 'S.W.');")

	cnt, err := dbLocal.Exec(sbSQL.String())
	if err != nil {
		fmt.Printf("db exec error: %v\n", err)
		return
	}

	fmt.Printf("insert ok, count: %v\n", cnt)
}

func DBCost() {
	t := time.Now().Unix()
	fmt.Printf("num of goroutine: %v\n", runtime.NumGoroutine())

	wg.Add(loopRemote)
	for i := 0; i < loopRemote; i++ {
		go func(n int) {
			queryRemote()
			wg.Done()
		}(i)
	}

	wg.Add(loop)
	for i := 0; i < loop; i++ {
		go func(n int) {
			queryLocal()
			wg.Done()
		}(i)
	}

	fmt.Printf("num of goroutine: %v\n", runtime.NumGoroutine())

	wg.Wait()

	fmt.Printf("num of goroutine: %v\n", runtime.NumGoroutine())
	fmt.Printf("time cost: %vs\n", time.Now().Unix()-t)
}

func queryRemote() string {
	tsql := "SELECT GameID, GameName FROM GameInfo;"
	_, err := dbRemote.Query(tsql)
	if err != nil {
		fmt.Printf("remote query error: %v\n", err)
		return err.Error()
	}

	return "ok"
}

func queryLocal() string {
	tsql := "SELECT * FROM Books;"
	_, err := dbLocal.Query(tsql)
	if err != nil {
		fmt.Printf("local qeury error: %v\n", err)
		return err.Error()
	}

	return "ok"
}

func Txexam() {
	tx, err := dbLocal.CommenceTransaction()
	if err != nil {
		fmt.Printf("fail to open transaction: %v\n", err)
		return
	}

	// v, e := tx.Query("SELECT id, user_name, price, create_date FROM weizhenexam.users where id= %d ", 1)
	v, e := tx.Exec("UPDATE weizhenexam.users SET user_name='BCA  12' WHERE id=%v or id = %d;", 12, 13)
	if e != nil {
		fmt.Printf("sql fail: %v\n", e)
		tx.Rollback()
		return
	}

	tx.Commit()

	fmt.Println(v)
}

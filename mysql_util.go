package swmysql

import (
	"database/sql"
	"errors"
	"strings"
)

func queryRtnParsing(rows *sql.Rows) []map[string]string {
	// defer func() {
	// 	err := rows.Close()
	// 	fmt.Printf("rows close err: %v\n", err)
	// }()
	defer rows.Close()

	columns, _ := rows.Columns()
	columnsCnt := len(columns)
	columnsPtr := make([]interface{}, columnsCnt)
	columnsVal := make([]interface{}, columnsCnt)

	for index := range columnsVal {
		columnsPtr[index] = &columnsVal[index]
	}

	rtnRowsMap := make([]map[string]string, 0)
	for rows.Next() {
		oneRowMap := make(map[string]string)
		rows.Scan(columnsPtr...)
		for index, column := range columnsVal {
			if column == nil {
				continue
			}
			oneRowMap[columns[index]] = string(column.([]byte))
		}

		rtnRowsMap = append(rtnRowsMap, oneRowMap)
	}

	return rtnRowsMap
}

func sqlcmdType(sqlcmd string, args ...interface{}) (int, error) {
	argsCnt := len(args)
	if argsCnt == 0 {
		return PLAINTEXT, nil
	}

	questCnt := strings.Count(sqlcmd, "?")
	if questCnt == 0 && argsCnt > 0 {
		return PLAINTEXTWITHARGS, nil
	}

	if questCnt != argsCnt {
		return UNDEFINECMD, errors.New("arguments not matched in call")
	}

	return PREPARED, nil
}
